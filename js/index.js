/**
 * Our Chart.js chart
 * @type {null} null initially, otherwise the type will be Chart
 */
let myChart = null;

/**
 * A boolean variable to help with showing and hiding the chart.
 * @type {boolean}
 */
let showingChart = true;

/**
 * Everything that happens once the document has finished loading.
 * Initializes all necessary events and calls the loadFilteredMeasurements() function.
 */
$(document).ready(function () {
    $(function () {
        $('#datetimepicker').datetimepicker({format: 'YYYY-MM-DD HH:mm:ss'});
    });

    loadFilteredMeasurements();

    $("#showButton").click(function () {
        loadFilteredMeasurements();
    });

    $("#btnToggleChart").click(function () {
        let button = document.getElementById('btnToggleChart');

        if (showingChart) {
            button.className = 'glyphicon glyphicon-chevron-down btn btn-primary pull-right';
            showingChart = false;
            $("#myChart").hide();
        } else {
            button.className = 'glyphicon glyphicon-chevron-up btn btn-primary pull-right';
            showingChart = true;
            $("#myChart").show();
        }
    });

    $("#showHumi").change(() => preventNoneChecked("Humi"));
    $("#showTemp").change(() => preventNoneChecked("Temp"));

    function preventNoneChecked(box) {
        if (humiIsChecked() == false && tempIsChecked() == false) {
            document.getElementById("show" + box + "Label").className = "btn btn-default active";
            document.getElementById("show" + box).checked = true;        }
    }

});

/**
 * Calls GET api/measurement to get all measurements from our REST api,
 * displays the results of parseMeasurementTable(data) in the field with the id "measurement"
 * and calls renderChart(data).
 */
function loadAllMeasurements() {
    $.get("api/measurement", function (data) {
        $("#measurement").html(parseMeasurementTable(data));
        renderChart(data);
    });
}

/**
 * Parses the Table of measurements for the given data.
 * The data will be shown from last to first.
 * @param data The data to be parsed
 * @returns {string} A string of the entire table to be displayed somewhere.
 */
function parseMeasurementTable(data) {
    let tmp = "";

    // Show the data from back to front to show the newest measurements first
    for (let i = (data.length - 1); i >= 0; i--) {
        tmp += '<tr>';
        tmp += '<td>' + data[i].timestamp + '</td>';
        tmp += '<td>' + data[i].temperature + '°C</td>';
        tmp += '<td>' + data[i].humidity + '%</td>';
        tmp += '<td>';
        tmp += '<a class="btn btn-info" href="index.php?r=measurement/view&id=' + data[i].id + '"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;';
        tmp += '<a class="btn btn-primary" href="index.php?r=measurement/update&id=' + data[i].id + '"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;';
        tmp += "<a class='btn btn-danger' href='index.php?r=measurement/delete&id=" + data[i].id + "'><span class=\"glyphicon glyphicon-remove\"</a>";
        tmp += '</td>';
        tmp += '</tr>';
    }
    return tmp;
}

/**
 * Returns the current checked value of the showTemp element
 * @returns {boolean|checked|*} Current 'checked' value
 */
function tempIsChecked() {
    return document.getElementById("showTemp").checked;
}

/**
 * Returns the current checked value of the showHumi element
 * @returns {boolean|checked|*}
 */
function humiIsChecked() {
    return document.getElementById("showHumi").checked;
}

/**
 * Destroys current chart if something has been drawn already and
 * renders the chart new into the 'myChart' canvas.
 * Which datasets are rendered depends on the results of humiIsChecked() and tempIsChecked().
 * @param d The data to be rendered
 */
function renderChart(d) {

    if (myChart != null) {
        myChart.destroy();
    }

    let ctx = document.getElementById("myChart").getContext('2d');

    let labels = d.map(e => e.timestamp);
    let temperature = d.map(e => e.temperature);
    let humidity = d.map(e => e.humidity);

    let humidityDataset = getDataset("Humidity", humidity, "rgb(255, 142, 159)");
    let temperatureDataset = getDataset("Temperature", temperature, "rgb(75, 192, 192)");

    let showTemp = tempIsChecked();
    let showHumi = humiIsChecked();

    let datasets;

    if (showTemp == true && showHumi == true) {
        datasets = [temperatureDataset, humidityDataset];
    }
    else if (showTemp == true) {
        datasets = [temperatureDataset];
    }
    else if (showHumi == true) {
        datasets = [humidityDataset];
    }

    myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: datasets
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

/**
 * Returns a properly formatted dataset with the given labe, data and color values.
 * @param label The Label of the dataset (= What the dataset is described as)
 * @param data The actual data of the dataset
 * @param color The color of the dataset
 * @returns {{label: *, data: *, borderColor: *, backgroundColor: *, fill: boolean, lineTension: number}} The dataset.
 */
function getDataset(label, data, color) {
    return {
        label: label,
        data: data,
        borderColor: color,
        backgroundColor: color,
        fill: false,
        lineTension: 0.1
    };
}

/**
 * Loads the filtered measurements if a filter is selected.
 * Otherwise this function will call loadAllMeasurements()
 */
function loadFilteredMeasurements() {
    if (document.querySelector('input[name="scope"]:checked') != null) {
        let filter = document.querySelector('input[name="scope"]:checked').value;
        let startDate = document.getElementById("datePicker").value;

        if (filter == "" || startDate == "") {
            loadAllMeasurements();
        } else {
            $.get("api/measurement/" + filter + "/" + startDate, function (data) {
                $("#measurement").html(parseMeasurementTable(data));
                renderChart(data);
            })
        }
    }
}