<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 26.02.2018
 * Time: 10:19
 */

require_once('controllers/MeasurementRESTController.php');

try {
    (new MeasurementRESTController())->handleRequest();
} catch (Exception $e) {
    header("HTTP/1.1 400 " . $e->getMessage());
    echo json_encode($e->getMessage());
}
