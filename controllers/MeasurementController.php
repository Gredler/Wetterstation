<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 26.02.2018
 * Time: 11:38
 */

require_once "Controller.php";
require_once "models/Measurement.php";

class MeasurementController extends Controller
{
    /**
     * @param $route array, e.g. [credentials, view]
     */
    public function handleRequest($route)
    {
        $operation = sizeof($route) > 1 ? $route[1] : 'index';
        $id = isset($_GET['id']) ? $_GET['id'] : 0;

        if ($operation == 'index') {
            $this->actionIndex();
        } elseif ($operation == 'view') {
            $this->actionView($id);
        } elseif ($operation == 'create') {
            $this->actionCreate();
        } elseif ($operation == 'update') {
            $this->actionUpdate($id);
        } elseif ($operation == 'delete') {
            $this->actionDelete($id);
        } else {
            Controller::showError("Page not found", "Page for operation " . $operation . " was not found!");
        }
    }

    public function actionIndex()
    {
        $model = Measurement::getAll();
        $this->render('measurement/index', $model);
    }

    public function actionView($id)
    {
        $model = Measurement::get($id);
        $this->render('measurement/view', $model);
    }

    public function actionCreate()
    {
        $model = new Measurement(null, '', '', '', '');

        if (!empty($_POST)) {
            $model->timestamp = isset($_POST['timestamp']) ? $_POST['timestamp'] : null;
            $model->temperature = isset($_POST['temperature']) ? $_POST['temperature'] : null;
            $model->humidity = isset($_POST['humidity']) ? $_POST['humidity'] : null;

            if ($model->save()) {
                $this->redirect('measurement/view/&id=' . $model->id);
                return;
            }
        }

        $this->render('measurement/create', $model);
    }

    public function actionUpdate($id)
    {
        $model = Measurement::get($id);

        if (!empty($_POST)) {
            $model->timestamp = isset($_POST['timestamp']) ? $_POST['timestamp'] : null;
            $model->temperature = isset($_POST['temperature']) ? $_POST['temperature'] : null;
            $model->humidity = isset($_POST['humidity']) ? $_POST['humidity'] : null;

            if ($model->save()) {
                $this->redirect('measurement/view/&id=' . $model->id);
                return;
            }
        }

        $this->render('measurement/update', $model);
    }

    public function actionDelete($id)
    {
        if (!empty($_POST)) {
            Measurement::delete($id);
            $this->redirect('measurement/index');
            return;
        }

        $this->render('measurement/delete', Measurement::get($id));
    }
}