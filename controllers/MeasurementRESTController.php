<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 26.02.2018
 * Time: 12:08
 */


require_once 'RESTController.php';
require_once 'models/Measurement.php';

class MeasurementRESTController extends RESTController
{
    public function handleRequest()
    {
        switch ($this->method) {
            case 'GET':
                $this->handleGETRequest();
                break;
            case 'POST':
                $this->handlePOSTRequest();
                break;
            case 'PUT':
                $this->handlePUTRequest();
                break;
            case 'DELETE':
                $this->handleDELETERequest();
                break;
            default:
                $this->response('Method Not Allowed', 405);
                break;
        }
    }

    /**
     * get single/all measurement or search credentials
     * all credentials: GET api.php?r=measurement
     * single credentials: GET api.php?r=measurement/25 -> args[0] = 25
     * search credentials: GET api.php?r=measurement/day/xxx -> verb = search, args[0] = xxx
     */
    private function handleGETRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 0) {
            $model = Measurement::getAll();
            $this->response($model);
        } else if ($this->verb == null && sizeof($this->args) == 1) {
            $model = Measurement::get($this->args[0]);
            $this->response($model);
        } else if (($this->verb == 'day' || $this->verb == 'month' || $this->verb == 'year' ) && sizeof($this->args) == 1) {
            $model = Measurement::getAll($this->args[0], $this->verb);
            $this->response($model);
        } else {
            $this->response("Bad request", 400);
        }
    }

    /**
     * create credentials: POST api.php?r=measurement
     */
    private function handlePOSTRequest()
    {
        $model = new Measurement();
        $model->timestamp = isset($this->file['timestamp']) ? $this->file['timestamp'] : null;
        $model->temperature = isset($this->file['temperature']) ? $this->file['temperature'] : null;
        $model->humidity = isset($this->file['humidity']) ? $this->file['humidity'] : null;

        if ($model->save()) {
            $this->response("OK", 201);
        } else {
            $this->response($model->errors, 400);
        }
    }

    /**
     * update credentials: PUT api.php?r=measurement/25 -> args[0] = 25
     */
    private function handlePUTRequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {

            $model = Measurement::get($this->args[0]);
            $model->timestamp = isset($this->file['timestamp']) ? $this->file['timestamp'] : null;
            $model->temperature = isset($this->file['temperature']) ? $this->file['temperature'] : null;
            $model->humidity = isset($this->file['humidity']) ? $this->file['humidity'] : null;


            if ($model->save()) {
                $this->response("OK", 200);
            } else {
                $this->response($model->errors, 400);
            }

        } else {
            $this->response("Not Found", 404);
        }
    }

    /**
     * delete credentials: DELETE api.php?r=measurement/25 -> args[0] = 25
     */
    private function handleDELETERequest()
    {
        if ($this->verb == null && sizeof($this->args) == 1) {
            Measurement::delete($this->args[0]);
            $this->response("OK", 200);
        } else {
            $this->response("Not Found", 404);
        }
    }

}