<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 26.02.2018
 * Time: 11:43
 */

include "DatabaseObject.php";

class Measurement implements DatabaseObject, JsonSerializable
{
    private $id;
    private $timestamp;
    private $temperature;
    private $humidity;
    private $errors;


    /**
     * Measurement constructor.
     * @param $id
     * @param string $timestamp
     * @param string $temperature
     * @param string $humidity
     */
    public function __construct($id = 0, $timestamp = '', $temperature = '', $humidity = '')
    {
        $this->id = $id;
        $this->timestamp = $timestamp;
        $this->temperature = $temperature;
        $this->humidity = $humidity;
        $this->errors = [];
    }


    /**
     * Validates timestamp, temperature and humidity
     * @return boolean True if valid, otherwise false
     */
    public function validate()
    {
        return $this->validateTimestamp() &
            $this->validateNumberValueHelper("Temperature", "temperature", $this->temperature, 1000, -50) &
            $this->validateNumberValueHelper("Humidity", "humidity", $this->humidity, 100, 0);
    }

    /**
     * create or update an object
     * @return boolean true on success
     */
    public function save()
    {
        if ($this->validate()) {
            if ($this->id != null && $this->id > 0) {
                $this->update();
            } else {
                $this->id = $this->create();
            }

            return true;
        }

        return false;
    }

    /**
     * Creates a new object in the database
     * @return integer ID of the newly created object (lastInsertId)
     */
    public function create()
    {
        $db = Database::connect();
        $sql = "INSERT INTO measurement (timestamp, temperature, humidity) values(?, ?, ?)";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->timestamp, $this->temperature, $this->humidity));
        $lastId = $db->lastInsertId();
        Database::disconnect();
        return $lastId;
    }

    /**
     * Saves the object to the database
     */
    public function update()
    {
        $db = Database::connect();
        $sql = "UPDATE measurement set timestamp = ?, temperature = ?, humidity = ? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($this->timestamp, $this->temperature, $this->humidity, $this->id));
        Database::disconnect();
    }

    /**
     * Get an object from database
     * @param integer $id
     * @return object single object or null
     */
    public static function get($id)
    {
        $db = Database::connect();
        $sql = "SELECT * FROM measurement where id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        $d = $stmt->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();

        if ($d == null) {
            return null;
        } else {
            return new Measurement($id, $d['timestamp'], $d['temperature'], $d['humidity']);
        }
    }

    /**
     * Get an array of objects from database
     * @param null $startDay
     * @param null $filter
     * @return array array of objects or empty array
     */
    public static function getAll($startDay = null, $filter = null)
    {
        $measurements = [];
        $db = Database::connect();

        if ($filter != null && $startDay != null) {
            $sql = "SELECT * FROM measurement WHERE timestamp between ? and (? + interval '1' $filter ) ORDER BY timestamp ASC";
        } else {
            $sql = "SELECT * FROM measurement ORDER BY timestamp ASC";
        }

        $stmt = $db->prepare($sql);
        $stmt->execute(array($startDay, $startDay));
        $data = $stmt->fetchAll();
        Database::disconnect();

        foreach ($data as $d) {
            $measurements[] = new Measurement($d['id'], $d['timestamp'], $d['temperature'], $d['humidity']);
        }
        return $measurements;
    }

    /**
     * Deletes the object from the database
     * @param integer $id
     */
    public static function delete($id)
    {
        $db = Database::connect();
        $sql = "DELETE FROM measurement WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->execute(array($id));
        Database::disconnect();
    }

    /**
     * A helper for validating numerical values
     * @param $label
     * @param $key
     * @param $value
     * @param $maxValue
     * @param $minValue
     * @return bool
     */
    private function validateNumberValueHelper($label, $key, $value, $maxValue, $minValue)
    {
        if ($value > $maxValue) {
            $this->errors[$key] = "$label too high, only values between . $minValue and $maxValue allowed.";
            return false;
        } else if ($value < $minValue) {
            $this->errors[$key] = "$label too low, only values between . $minValue and $maxValue allowed.";
            return false;
        } else {
            unset($this->errors[$key]);
            return true;
        }
    }

    /**
     * @return bool
     */
    private function validateTimestamp()
    {
        if ($this->timestamp != null) {
            if ($this->id == null || $this->id <= 0) {
                if ($this->timestamp >= date("Y-m-d", time())) {
                    unset($this->errors['timestamp']);
                    return true;
                }
                $this->errors['timestamp'] = "Timestamp can't be in the past.";
                return false;
            } else {
                $measurement = Measurement::get($this->id);

                if ($measurement !== null) {
                    unset($this->errors['timestamp']);
                    return true;
                }
                $this->errors['timestamp'] = "An error occurred, please ty again.";
                return false;

            }
        }
        $this->errors['timestamp'] = "Please enter a timestamp.";
        return false;
    }

    /**
     * JsonSerializable implementation
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        //return get_object_vars($this);
        return [
            "id" => $this->id,
            "timestamp" => $this->timestamp,
            "temperature" => $this->temperature,
            "humidity" => $this->humidity
        ];
    }

    /**
     * Getter for some private attributes
     * @param $property
     * @return mixed $property
     */
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
        return null;
    }

    /**
     * Setter for some private attributes
     * @param $property
     * @param $value
     * @return mixed $name
     */
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

}