<?php

class Database
{
    private static $dbName = 'wetterstation_gredler';
    private static $dbHost = 'localhost';
    private static $dbUsername = 'root';
    private static $dbUserPassword = 'password1!';

    private static $conn = null;

    public function __construct()
    {
        exit('Init function is not allowed');
    }

    public static function connect()
    {
        // One connection through whole application
        if (null == self::$conn) {
            try {
                self::$conn = new PDO("mysql:host=" . self::$dbHost . ";" . "dbname=" . self::$dbName, self::$dbUsername, self::$dbUserPassword);
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
        self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$conn;
    }

    public static function disconnect()
    {
        self::$conn = null;
    }

    public static function install()
    {
        $conn = new mysqli(self::$dbHost, self::$dbUsername, self::$dbUserPassword);
        // Check connection
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        // Create database
        $sql = file_get_contents("init.sql", FILE_USE_INCLUDE_PATH);
        if ($conn->multi_query($sql) === TRUE) {
            echo "Created database successfully.";
        } else {
            echo "An error occurred while attempting to create the database: " . $conn->error;
        }

        $conn->close();
    }
}

?>