<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <title>Wetterstation</title>

    <link rel="stylesheet" href="/Wetterstation/css/bootstrap.min.css">
    <link rel="stylesheet" href="/Wetterstation/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/Wetterstation/css/datetimepicker.min.css">

    <script src="/Wetterstation/js/jquery.js"></script>
    <script src="/Wetterstation/js/moment.js"></script>
    <script src="/Wetterstation/js/bootstrap/transition.js"></script>
    <script src="/Wetterstation/js/bootstrap/collapse.js"></script>
    <script src="/Wetterstation/js/bootstrap/bootstrap.min.js"></script>
    <script src="/Wetterstation/js/bootstrap/datetimepicker.min.js"></script>
    <script src="/Wetterstation/js/index.js"></script>
    <script src="/Wetterstation/js/chart.js"></script>
</head>
<body>
