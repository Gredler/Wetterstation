<div class="container">
    <h2>Delete Measurement</h2>

    <form class="form-horizontal" action="index.php?r=measurement/delete&id=<?= $model->id ?>" method="post">
        <input type="hidden" name="id" value="<?php echo $model->id; ?>"/>
        <p class="alert alert-error">Do you really want to delete the measurement
            <?= $model->timestamp ?>?</p>
        <div class="form-actions">
            <button type="submit" class="btn btn-danger">Delete</button>
            <a class="btn btn-default" href="index.php?r=measurement/index">Cancel</a>
        </div>
    </form>

</div> <!-- /container -->