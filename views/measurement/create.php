<div class="container">
    <div class="row">
        <h2>Create Measurement</h2>
    </div>
    <?php
//    var_dump(date(DATE_ATOM, $model->timestamp));
    ?>

    <form class="form-horizontal" action="index.php?r=measurement/create" method="post">

        <div class="row">
            <div class="col-md-5">
                <div class="form-group required <?php echo !empty($model->errors['timestamp']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Timestamp *</label>
                    <div class="container">
                        <div class="row">
                            <div class='col-sm-5'>
                                <div class="form-group">
                                    <div class='input-group date' id='datetimepicker'>
                                        <input type='text' class="form-control" id="datePicker" name="timestamp"
                                               value="<?= $model->timestamp ?>"/>
                                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php if (!empty($model->errors['timestamp'])): ?>
                        <div class="help-block"><?= $model->errors['timestamp'] ?></div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-md-5">
                <div class="form-group required <?php echo !empty($model->errors['temperature']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Temperature *</label>
                    <input type="number" step="0.01" class="form-control" name="temperature" max="1000" min="-50" required
                           value="<?= $model->temperature ?>">

                    <?php if (!empty($model->errors['temperature'])): ?>
                        <div class="help-block"><?= $model->errors['temperature'] ?></div>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-md-2"></div>

            <div class="col-md-5">
                <div class="form-group required <?php echo !empty($model->errors['humidity']) ? 'has-error' : ''; ?>">
                    <label class="control-label">Humidity *</label>
                    <input type="number" step="0.01" class="form-control" name="humidity" max="100" min="0" required
                           value="<?= $model->humidity ?>">

                    <?php if (!empty($model->errors['humidity'])): ?>
                        <div class="help-block"><?= $model->errors['humidity'] ?></div>
                    <?php endif; ?>
                </div>
            </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Create</button>
            <a class="btn btn-default" href="index.php?r=measurement/index">Cancel</a>
        </div>
    </form>

</div> <!-- /container -->