<div class="container">

    <h1>Wetterstation</h1>

    <div class="row">
        <button id="btnToggleChart" class="glyphicon glyphicon-chevron-up btn btn-primary pull-right"></button>
        <div class="col-md-12" id="graphContainer">
            <canvas id="myChart" height="125"></canvas>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-6">

            <div class="container">
                <div class="row">
                    <div class='col-sm-4'>
                        <div class="form-group">
                            <div class='input-group date' id='datetimepicker'>
                                <input type='text' class="form-control" id="datePicker" value="2018-01-01 00:00:00"/>
                                <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default active">
                                <input type="radio" name="scope" autocomplete="off" value="day" checked> Day
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="scope" autocomplete="off" value="month"> Month
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="scope" autocomplete="off" value="year"> Year
                            </label>
                        </div>

                        <button class="btn btn-primary" id="showButton" value="show">Show</button>


                    </div>

                    <div class="col-sm-4">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default active" id="showTempLabel">
                                <input type="checkbox" id="showTemp" checked> Show Temperature
                            </label>
                            <label class="btn btn-default active" id="showHumiLabel">
                                <input type="checkbox" id="showHumi" checked> Show Humidity
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>

        <div class="row">
            <div class="col-md-5">
                <a href="index.php?r=measurement/create" class="btn btn-success"><span
                            class="glyphicon glyphicon-plus"></span> Create New Measurement</a>
            </div>
        </div>
        <br>

        <div class="row">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Timestamp</th>
                    <th>Temperature</th>
                    <th>Humidity</th>
                    <th></th>
                </tr>
                </thead>
                <tbody id="measurement">
                </tbody>
            </table>
        </div>
    </div>

