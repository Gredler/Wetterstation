<div class="container">
    <h2>Beitrag anzeigen</h2>

    <p>
        <a class="btn btn-primary" href="index.php?r=measurement/update&id=<?= $model->id ?>">Update</a>
        <a class="btn btn-danger" href="index.php?r=measurement/delete&id=<?= $model->id ?>">Delete</a>
        <a class="btn btn-default" href="index.php">Return</a>
    </p>

    <table class="table table-striped table-bordered detail-view">
        <tbody>
        <tr>
            <th>Timestamp</th>
            <td><?= $model->timestamp ?></td>
        </tr>
        <tr>
            <th>Temperature</th>
            <td><?= $model->temperature ?>°C</td>
        </tr>
        <tr>
            <th>Humidity</th>
            <td><?= $model->humidity ?>%</td>
        </tr>
        </tbody>
    </table>
</div> <!-- /container -->
