<?php
/**
 * Created by PhpStorm.
 * User: Andi
 * Date: 26.02.2018
 * Time: 10:18
 */

// select route
$route = isset($_GET['r']) ? explode('/', trim($_GET['r'], '/')) : ['measurement'];
$controller = sizeof($route) > 0 ? $route[0] : 'measurement';


if ($controller == 'measurement') {
    require_once('controllers/MeasurementController.php');
    (new MeasurementController())->handleRequest($route);
} else {
    Controller::showError("Page not found", "Page for operation " . $controller . " was not found!");
}

